import {BaseComponent, bind, tracked} from '@epicentric/utilbelt-react';
import React, {createRef, forwardRef, ReactNode, Ref, RefObject} from 'react';
import {SimpleHotkey, SimpleHotkeyKeymap} from '@epicentric/simple-hotkey';
import {Omit} from 'utility-types';
import styled from 'styled-components';

interface InputPlusProps
    extends Omit<React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>, 'onChange'|'ref'>
{
    value?: string;
    onChange?: (value: string) => void;
    onInternalChange?: (value: string) => void;
    enabled?: boolean;
    componentRef?: Ref<InputPlusComponent>;
    forwardedRef?: Ref<HTMLDivElement>;
}

interface InputPlusState
{
    attached: boolean;
}

const InputPlusField = styled.div`
  display: inline-block;
  
  &:focus {
    outline: 0;
  }

  &:empty:before {
    content: attr(placeholder);
    pointer-events: none;
    opacity: .3;
  }
`;

export class InputPlusComponent extends BaseComponent<InputPlusProps, InputPlusState>
{
    @tracked
    private attached: boolean = true;

    private divRef: RefObject<HTMLDivElement> = createRef();

    private keymap: SimpleHotkeyKeymap = {
        'enter, escape': { callback: this.onHotkey, event: 'keydown' }
    };

    private get internalValue(): string
    {
        return this.divRef.current ? this.divRef.current.textContent || '' : '';
    }

    private set internalValue(value: string)
    {
        const current = this.divRef.current;
        
        if (current && current.textContent !== value)
        {
            current.textContent = value;
        }
    }

    public constructor(props: InputPlusProps, context: any)
    {
        super(props, context);

        this.didChange.subscribe(({prevState, state, prevProps, props}) => {
            if (state.attached && (prevProps.value !== props.value))
            {
                this.internalValue = props.value || '';
            }
        });
        
        // Intercept forwardedRef
        this.didChange.subscribe(({prevProps, props}) => {
            if (prevProps.forwardedRef === props.forwardedRef)
                return;
            
            this.updateForwardRef();
        });
    }

    public render(): ReactNode
    {
        // noinspection JSUnusedLocalSymbols
        const { value, onChange, onInternalChange, enabled, ...otherProps } = this.props;

        // noinspection PointlessBooleanExpressionJS
        return <SimpleHotkey keymap={this.keymap}>
            <InputPlusField
                {...otherProps}
                ref={this.onRefChange}
                contentEditable={enabled !== undefined ? !!enabled : true}
                onFocus={this.onFocus}
                onBlur={this.onBlur}
                onInput={this.onInput}
                onPaste={this.onPaste}
            />
        </SimpleHotkey>;
    }

    public blur(): void
    {
        const current = this.divRef.current;
        
        if (current)
        {
            current.blur();
        }
    }

    public detach(): void
    {
        this.attached = false;
    }

    public attach(keepInternalValue: boolean): void
    {
        if (this.attached)
            return;

        if (keepInternalValue)
        {
            if (this.props.onChange && this.props.value !== this.internalValue)
            {
                this.props.onChange(this.internalValue);
            }
        }
        else
        {
            this.internalValue = this.props.value || '';
        }

        this.attached = true;
    }
    
    private updateForwardRef()
    {
        const ref = this.divRef.current;
        const forwardedRef = this.props.forwardedRef;

        if (!forwardedRef)
            return;

        if (typeof forwardedRef === 'function')
            forwardedRef(ref);
        else if ('current' in forwardedRef)
            (forwardedRef as any).current = ref;
    }
    
    @bind
    private onRefChange(ref: HTMLDivElement|null)
    {
        (this.divRef as any).current = ref;
        
        this.updateForwardRef();
    }

    @bind
    private onFocus(event: React.FocusEvent<HTMLDivElement>)
    {
        this.detach();

        if (this.props.onFocus)
            this.props.onFocus(event);
    }

    @bind
    private onBlur(event: React.FocusEvent<HTMLDivElement>)
    {
        this.attach(true);

        if (this.props.onBlur)
            this.props.onBlur(event);
    }

    @bind
    private onInput(event: React.FormEvent<HTMLDivElement>)
    {
        if (this.props.onInternalChange)
            this.props.onInternalChange(this.internalValue);
    }

    @bind
    private onHotkey(event: KeyboardEvent, combo: string): void
    {
        event.preventDefault();

        switch (combo)
        {
            case 'enter':
                this.attach(true);

                this.blur();
                break;
            case 'escape':
                this.attach(false);

                this.blur();
                break;
        }
    }

    @bind
    private onPaste(event: React.ClipboardEvent<HTMLDivElement>): void
    {
        // Based on: https://stackoverflow.com/a/6804718 and https://stackoverflow.com/a/19269040

        // Stop data actually being pasted into div
        event.preventDefault();

        // Get pasted data via clipboard API
        let clipboardText = event.clipboardData.getData('text/plain');
        // Strip new lines
        clipboardText = clipboardText.replace('\n', ' ');

        // Insert it
        document.execCommand('insertText', false, clipboardText);
    }
}

export const InputPlus = styled(forwardRef<HTMLDivElement, InputPlusProps>((props, ref) => {
    return <InputPlusComponent {...props} forwardedRef={ref} ref={props.componentRef} />;
}))``;